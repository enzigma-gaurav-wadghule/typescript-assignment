import {expect} from 'chai'
import { describe } from 'mocha';
import {User} from '../model/User'
import * as register from './register'
import { retrieveUser } from './retrieveUser';

describe("this is test methods for node async",()=>{
    it("this method shoud register the user and add the data to the file",()=>{
        var obj = new User('gaurav','dsfkghkd', 'gaurav', 'wadghule','patas');
        register.newUser(obj)
        .then((res)=>{console.log('data added successfully')
        expect(res, 'true')})
        .catch((err)=>console.log(err));
        
    })

    it('this method should retrive the user',()=>{
        retrieveUser()
            .then((data)=>console.log(data))
            .catch((err)=>console.log(err))
    })

})