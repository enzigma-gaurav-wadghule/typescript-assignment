import * as file from 'fs'

export function retrieveUser() {

    return new Promise((resolve, reject) => {
        try {
            resolve(file.readFileSync('userDetails.json', 'utf8'))
        }
        catch (err) {
            reject(err)
        }
    })
}