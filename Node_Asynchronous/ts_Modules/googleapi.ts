import * as http from 'http'

export function getLocation(address: string) {

  var results;
  var url = 'http://www.mapquestapi.com/geocoding/v1/address?key=eyHAGSohVCwSMGKk5NGJ1Ibw1JAdCLZ3&location=' + address
  
  return new Promise((resolve,reject)=>{
    http.get(url, (resp: any) => {
      let data = '';
  
      // A chunk of data has been recieved.
      resp.on('data', (chunk: any) => {
        data += chunk;
      });
  
      // The whole response has been received. Print out the result.
      resp.on('end', () => {
        results = JSON.parse(data)
        resolve(results.results[0].locations[0].latLng);
      });
  
    }).on("error", (err: any) => {
      console.log("Error: " + err.message);
      reject();
    });
  })
  

}




