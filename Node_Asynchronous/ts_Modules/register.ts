import * as file from 'fs'

export function newUser(user : any){
    var fileData = file.readFileSync('userDetails.json','utf8');
    if(fileData.length == 0){
        fileData = fileData+'[';
    }
    else{
        fileData = fileData.substring(0,fileData.length-1);
        fileData = fileData + ','
    }
  
    return new Promise((resolve, reject) => {
       
            try{
                fileData = fileData + JSON.stringify(user) + ']';
               
            file.writeFileSync('userDetails.json',fileData)  
            resolve(true);
            }
            catch(err){
                reject(err);
            }
        
    })
}

