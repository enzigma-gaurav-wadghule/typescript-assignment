"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var User = /** @class */ (function () {
    function User(username, password, firstName, lastName, Address) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.Address = Address;
        this.password = password;
    }
    return User;
}());
exports.User = User;
