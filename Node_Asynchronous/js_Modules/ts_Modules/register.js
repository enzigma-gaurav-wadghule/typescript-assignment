"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var file = __importStar(require("fs"));
function newUser(user) {
    var fileData = file.readFileSync('userDetails.json', 'utf8');
    if (fileData.length == 0) {
        fileData = fileData + '[';
    }
    else {
        fileData = fileData.substring(0, fileData.length - 1);
        fileData = fileData + ',';
    }
    return new Promise(function (resolve, reject) {
        try {
            fileData = fileData + JSON.stringify(user) + ']';
            file.writeFileSync('userDetails.json', fileData);
            resolve();
        }
        catch (err) {
            reject(err);
        }
    });
}
exports.newUser = newUser;
