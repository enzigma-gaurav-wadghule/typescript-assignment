"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var readline = __importStar(require("readline-Sync"));
var User_1 = require("../model/User");
var register = __importStar(require("./register"));
var retrieveUser_1 = require("./retrieveUser");
var googleapi_1 = require("./googleapi");
console.log("-------Menu--------\n    1. Register user\n    2. retrive user\n    3. get your latitude and longitude");
var choice = (Number)(readline.question('Enter your choice'));
switch (choice) {
    case 1: {
        var username = (String)(readline.question("Enter username of user ="));
        var password = (String)(readline.question("Enter password of user ="));
        var firstname = (String)(readline.question("Enter firstname of user ="));
        var lastname = (String)(readline.question("Enter lastname of user ="));
        var address = (String)(readline.question("Enter address of user ="));
        var obj = new User_1.User(username, password, firstname, lastname, address);
        register.newUser(obj)
            .then(function () { console.log('data added successfully'); })
            .catch(function (err) { return console.log(err); });
        break;
    }
    case 2: {
        retrieveUser_1.retrieveUser()
            .then(function (data) { return console.log(data); })
            .catch(function (err) { return console.log(err); });
        break;
    }
    case 3: {
        googleapi_1.getLocation('baramati pune')
            .then(function (data) { return console.log(data); });
        break;
    }
}
