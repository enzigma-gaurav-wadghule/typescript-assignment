"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var http = __importStar(require("http"));
function getLocation(address) {
    var results;
    var url = 'http://www.mapquestapi.com/geocoding/v1/address?key=eyHAGSohVCwSMGKk5NGJ1Ibw1JAdCLZ3&location=' + address;
    return new Promise(function (resolve, reject) {
        http.get(url, function (resp) {
            var data = '';
            // A chunk of data has been recieved.
            resp.on('data', function (chunk) {
                data += chunk;
            });
            // The whole response has been received. Print out the result.
            resp.on('end', function () {
                results = JSON.parse(data);
                resolve(results.results[0].locations[0].latLng);
            });
        }).on("error", function (err) {
            console.log("Error: " + err.message);
            reject();
        });
    });
}
exports.getLocation = getLocation;
