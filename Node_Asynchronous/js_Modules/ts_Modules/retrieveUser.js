"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var file = __importStar(require("fs"));
function retrieveUser() {
    return new Promise(function (resolve, reject) {
        try {
            resolve(file.readFileSync('userDetails.json', 'utf8'));
        }
        catch (err) {
            reject(err);
        }
    });
}
exports.retrieveUser = retrieveUser;
