import * as mongoose from 'mongoose'
import {userSchema} from '../objects/user'
var userModel = mongoose.model('user', userSchema);

module.exports = userModel;