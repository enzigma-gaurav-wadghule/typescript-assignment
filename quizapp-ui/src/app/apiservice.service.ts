import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ApiServices {
 


  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };
  constructor(private http: HttpClient) { }

  newUser (user : any) : Observable<any> {
    console.log('inside register user service');
    return this.http.post('http://localhost:3000/register',user, this.httpOptions)
  }

  login (user : any) : Observable<any>{
    console.log('inside login user service');
    return this.http.post('http://localhost:3000/login',user, this.httpOptions)
  }
}
