

import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';

import { user } from '../models/user';
import { ApiServices } from '../apiservice.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router'
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit{
 
  user : any = {};

constructor(private router: Router, private registeruserService: ApiServices, private toastr: ToastrService){

  }

  onSubmit(form) { 
    console.log('user is',this.user);
    this.registeruserService.newUser(this.user)
    .subscribe((res)=>{ 
      if(res.status==="user created"){
       console.log('success')
      this.toastr.success('user created!'); 
      form.reset();
      this.router.navigate(['home']);
      }
    },(err)=>{
      this.toastr.error('User already exist!')
    });
  }
  

  ngOnInit(){

 }

}