import { Component, OnInit } from '@angular/core';
import { ApiServices } from '../apiservice.service'
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user : any = {};
  constructor(private router: Router, private registeruserService: ApiServices, private toastr: ToastrService) { }

  ngOnInit() {
  }

  onSubmit(){
    console.log(this.user);
    this.registeruserService.login(this.user)
    .subscribe((res)=>{
      if(res.status==="logged in"){
        console.log('logged in');
        this.toastr.success('Logged in successfully!');
        this.router.navigate(['dashboard']);
      }
    },(err)=>{
      console.log(err);
      this.toastr.error('Wrong credentials!');
    })
  }

}
