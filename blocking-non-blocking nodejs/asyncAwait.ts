import { addition, asyncDivision, mul } from "./promises";

async function main(){
    let divisionResult = await asyncDivision(100,20)
    if(divisionResult)
    console.log(divisionResult)
    let additionResult = await addition(divisionResult)
    if(additionResult)
    console.log(additionResult)
    let mulResult = await mul(additionResult)
    if(mulResult)
    console.log(mulResult);
}

