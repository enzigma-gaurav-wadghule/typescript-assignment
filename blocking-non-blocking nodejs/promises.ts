
export function asyncDivision(number1: any, number2: any) {
    return new Promise((resolve, reject) => {
      
            if(typeof number1 ==='number'&& typeof number2==='number'){
                let res = number1/number2;
                resolve(res);
             }
            else
            reject('please enter the valid number');
    });
}

export function addition(num1:any){
    return new Promise((resolve,reject)=>{
        resolve(num1 + 5);
    })
}

export function mul(num1:any){
    return new Promise((resolve, reject)=>{
        resolve(num1 * 10);
    })
}

asyncDivision(100,20)
.then((res)=>{
    console.log(res)
    mul(res)
    .then((res)=>{
        console.log(res)
        addition(res)
        .then((res)=>{
            console.log(res)
        })
    })
})
.catch((err)=>{console.log(err)})

